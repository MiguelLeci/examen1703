@extends('layouts.app')


@section('content')
    <h1>Nuevo Libro</h1>
    <form action="/" method="post">
        {{ csrf_field() }}
        
        
        <div class="form-group">
            <label>Titulo</label>
            <input type="text" name="title" >
        </div>
        <div class="form-group">
            <label>Paginas</label>
            <input type="text" name="pages" >
        </div>
        <div class="form-group">
            <label>Año</label>
            <input type="text" name="year" >
        </div>

        <div class="form-group">
            <label>genero</label>
            <select name="gender_id">
                @foreach ($genders as $gender)
                    <option value="{{ $gender['id'] }}">{{ $gender['name'] }}</option>
                @endforeach
            </select>
        </div>
        
        <div class="form-group">
            <label>User Id</label>
            <input type="text" name="user_id" value="{{  $user['id'] }}" readonly="readonly">
        </div>
        @if ($errors->first('title'))
            <li>Error con titulo es Obligatorio</li>
        @endif
        @if ($errors->first('pages'))
            <li>Error con Paginas es Obligatorio</li>
        @endif
        @if ($errors->first('year'))
            <li>Error con año es Obligatorio</li>
        @endif
        <div class="form-group">
            <label></label>
            <input type="submit" value="Guardar" class="btn btn-default"><br>
        </div>
    </form>
@endsection