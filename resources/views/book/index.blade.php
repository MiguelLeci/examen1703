@extends('layouts.app')

@section('content')
    <h1>Lista de Libros</h1>
    <p><a href="/create">Nueva Libro</a></p>

    <table class="table table-striped">
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Paginas</th>
            <th>Año</th>
            <th>Genero</th>
            <th>Creador</th>
        </tr>
        @foreach ($books as $book)
        <tr data-id="{{ $book['id'] }}">
            <td>{{ $book['id'] }}</td>
            <td>{{ $book['title'] }}</td>
            <td>{{ $book['pages'] }}</td>
            <td>{{ $book['year'] }}</td>
            
            <td>{{ $book->gender->name }}</td>
            <td>{{ $book->user->name }}</td>
            
            <td>
               

                    <form method="post" action="/borrar/{{ $book->id }}">
                            @can('delete',  $book)
                            {{ csrf_field() }}
                            <input type="submit" value="Borrar">
                            <input type="hidden" name="_method" value="DELETE">
                            @endcan
                    <a href="/show/{{ $book->id }}">Ver</a>
                    </form>

                </td>
            

        </tr>  
        @endforeach
    </table>
    {!! $books->render() !!}
@endsection
