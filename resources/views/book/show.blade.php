@extends('layouts.app')

@section('content')

    <h1>Detalles de libro {{ $book['id'] }}</h1>  
    
    <table class='table table-striped'>
        <thead>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <td>Paginas</td>
                <td>Año</td>
                @foreach ($book->authors as $author )

                        @if ($book['author_id'] == $book->author['id'])
                            <td>Autor</td>
                        @endif
                    @endforeach
                
                <td>Genero</td>
                <td>Creador</td>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $book['id'] }}</td>
                <td>{{ $book['title'] }}</td>
                <td>{{ $book['pages'] }}</td>
                <td>{{ $book['year'] }}</td>
                
                    @foreach ($book->authors as $author )

                        @if ($book['author_id'] == $book->author['id'])
                            <td>{{ $author['name'] }}</td>
                        @endif
                    @endforeach
                
                <td>{{ $book->gender->name }}</td>
                <td>{{ $book->user->name }}</td>
                
            </tr>
        </tbody>

        
    </table> 
    </div>     
@endsection

