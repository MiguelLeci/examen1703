<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource('/', 'Resource@index');
Route::resource('/borrar', 'Resource@destroy');
Route::resource('/show', 'Resource@show');
Route::resource('/', 'Resource');
