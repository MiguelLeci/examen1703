<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Gender;
use App\User;
use App\Author;
use App\Providers\AuthServiceProvider;

class Resource extends Controller
{
    public function index(Request $request)
    {
        $books = Book::paginate(10);
        

        return view('book.index', ['books' => $books]);
        
    }

    //FUNCION CREAR
    public function create(Request $request)
    {
        
    
        $this->middleware('auth');
    
        $genders = Gender::all();
        $user = $request->user();
        return view('book.create', ['user' => $user],['genders' => $genders]);
    }

    //El GUARDADO DE CREAR
    public function store(Request $request)
    {
        $this->validate($request, [
        'title' => 'required',
        'pages' => 'required',
        'year' => 'required'
        ]);

        $book = new Book($request->all());
        $book->save();

        return redirect('/');
    }
    

    public function show(Request $request, $id)
    {
        $book= Book::findOrFail($id);
        
            return view('book.show', ['book' => $book]);
        
    }

   
   
    public function destroy($id, Request $request) 
    {
        Book::find($id)->delete();
        return redirect('/');
    }
}
