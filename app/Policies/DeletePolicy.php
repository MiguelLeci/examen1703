<?php

namespace App\Policies;

use App\User;
use App\Book;

use Illuminate\Auth\Access\HandlesAuthorization;

class DeletePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
   public function delete(User $user, Book $book)
    {
        
        if ([$user['id'] == $book['user_id']]) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
